=============================
{{ cookiecutter.project_name }}
=============================

.. image:: https://gitlab.com/{{ cookiecutter.gitlab_username }}/{{ cookiecutter.repo_name }}/badges/master/coverage.svg
   :target: https://gitlab.com/{{ cookiecutter.gitlab_username }}/{{ cookiecutter.repo_name }}/pipelines
.. image:: https://gitlab.com/{{ cookiecutter.gitlab_username }}/{{ cookiecutter.repo_name }}/badges/master/build.svg
   :target: https://gitlab.com/{{ cookiecutter.gitlab_username }}/{{ cookiecutter.repo_name }}/commits/master
.. image:: https://badge.fury.io/py/{{ cookiecutter.repo_name }}.svg
    :target: https://badge.fury.io/py/{{ cookiecutter.repo_name }}

{{ cookiecutter.project_short_description}}

Documentation
-------------

The full documentation is at https://{{ cookiecutter.gitlab_username }}.gitlab.io/{{ cookiecutter.repo_name }}/

Quickstart
----------

Install {{ cookiecutter.project_name }}::

    pip install {{ cookiecutter.repo_name }}

Add it to your `INSTALLED_APPS`:

.. code-block:: python

    INSTALLED_APPS = (
        ...
        '{{ cookiecutter.app_name }}.apps.{{ cookiecutter.app_config_name }}',
        ...
    )

Add {{ cookiecutter.project_name }}'s URL patterns:

.. code-block:: python

    from {{ cookiecutter.app_name }} import urls as {{ cookiecutter.app_name }}_urls


    urlpatterns = [
        ...
        url(r'^', include({{ cookiecutter.app_name }}_urls)),
        ...
    ]

Features
--------

* TODO

Running Tests
-------------

Does the code actually work?

::

    source <YOURVIRTUALENV>/bin/activate
    (myenv) $ pip install tox
    (myenv) $ tox

